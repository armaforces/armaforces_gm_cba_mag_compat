@echo off
call tools\build\fnc.bat :argparse %1 %2 %3 %4 %5 %6 %7

:: Possible arguments:
::
:: "-sign"      Require sign, script will fail if private key is not present
:: "-nosign"    Do not sign
:: "-ds"        Use DSSign instead of armake2 for signing
::
:: Mod settings
set private_key=ArmaForces_gm_cba_mag_compat.biprivatekey
set public_key=ArmaForces_gm_cba_mag_compat.bikey
set mod_base=armaforces_gm_cba_mag_compat

:: Build folders
call tools\build\fnc.bat :build_folder addons

exit /B %errorlevel%
