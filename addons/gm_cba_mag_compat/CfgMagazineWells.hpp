class CfgMagazineWells
{
    class CBA_762x51_LINKS {        // M13 Links for M60, M240, MG3 (DM6/DM60)
        gm_belts[] = {
            "gm_120Rnd_762x51mm_b_t_DM21_mg3_grn",
            "gm_120Rnd_762x51mm_b_t_DM21A1_mg3_grn",
            "gm_120Rnd_762x51mm_b_t_DM21A2_mg3_grn"
        };
    };

    class CBA_762x54R_LINKS {       // Links for PK, PKM, and similar
        gm_belts[] = {
            "gm_100Rnd_762x54mm_b_t_t46_pk_grn",
            "gm_100Rnd_762x54mm_api_b32_pk_grn"
        };
    };

    class CBA_9x19_UZI { // TODO, this is not from CBA. Find correct CBA class
        gm_mags[] = {
            "gm_32Rnd_9x19mm_b_DM51_mp2_blk",
            "gm_32Rnd_9x19mm_b_DM11_mp2_blk"
        };
    };

    class CBA_762x39_AK {           // Standard AK-47/AKM magazines
        gm_mags[] = {
            "gm_30Rnd_762x39mm_b_M43_ak47_blk",
            "gm_30Rnd_762x39mm_b_t_M43_ak47_blk"
        };
    };
    class CBA_762x39_RPK {          // 40/45/75rnd RPK magazines
        gm_mags[] = {
            "gm_75Rnd_762x39mm_b_M43_ak47_blk",
            "gm_75Rnd_762x39mm_b_t_M43_ak47_blk"
        };
    };

    class CBA_545x39_AK {           // Standard AK-74 magazines
        gm_mags[] = {
            "gm_30Rnd_545x39mm_b_7N6_ak74_prp",
            "gm_30Rnd_545x39mm_b_t_7T3_ak74_prp",
            "gm_30Rnd_545x39mm_b_7N6_ak74_org",
            "gm_30Rnd_545x39mm_b_t_7T3_ak74_org"
        };
    };
    class CBA_545x39_RPK {        // 45rnd RPK-74
        gm_mags[] = {
            "gm_45Rnd_545x39mm_b_7N6_ak74_prp",
            "gm_45Rnd_545x39mm_b_t_7T3_ak74_prp",
            "gm_45Rnd_545x39mm_b_7N6_ak74_org",
            "gm_45Rnd_545x39mm_b_t_7T3_ak74_org"
        };
    };

    class CBA_762x51_G3 {         // H&K G3
        gm_mags[] = {
            "gm_20Rnd_762x51mm_b_t_DM21_g3_blk",
            "gm_20Rnd_762x51mm_b_t_DM21A1_g3_blk",
            "gm_20Rnd_762x51mm_b_t_DM21A2_g3_blk",
            "gm_20Rnd_762x51mm_b_DM111_g3_blk",
            "gm_20Rnd_762x51mm_b_DM41_g3_blk",
            "gm_20Rnd_762x51mm_ap_DM151_g3_blk",
            "gm_20Rnd_762x51mm_b_t_DM21_g3_des",
            "gm_20Rnd_762x51mm_b_t_DM21A1_g3_des",
            "gm_20Rnd_762x51mm_b_t_DM21A2_g3_des",
            "gm_20Rnd_762x51mm_b_DM111_g3_des",
            "gm_20Rnd_762x51mm_b_DM41_g3_des",
            "gm_20Rnd_762x51mm_ap_DM151_g3_des"
        };
    };
    class CBA_RPG7 {                // RPG-7
        GM_rockets[] = {
            "gm_1Rnd_40mm_heat_pg7v_rpg7"
        };
    };
};
