class CfgPatches
{
    class armaforces_gm_cba_mag_compat
    {
        name = "ArmaForces - GM CBA Mag Compat";
        units[] = {};
        weapons[] = {};
        requiredVersion = 0.1;
        requiredAddons[] = {
            "cba_main",
            "gm_weapons_launchers_rpg7",
            "gm_weapons_rifles_g3",
            "gm_weapons_rifles_ak74",
            "gm_weapons_rifles_ak47",
            "gm_weapons_machinepistols_gm_mp2",
            "gm_weapons_machineguns_pk",
            "gm_weapons_machineguns_mg3"
        };
        author = "3Mydlo3, veteran29";
    };
};

#include "CfgMagazineWells.hpp"
#include "CfgWeapons.hpp"
