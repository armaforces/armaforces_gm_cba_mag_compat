class CfgWeapons {
    class gm_machineGun_base;
    class gm_rifle_base;
    class gm_launcher_base;
    // MG3
    class gm_mg3_base: gm_machineGun_base {
        magazineWell[] += {"CBA_762x51_LINKS"};
    };
    class gm_mg3_veh_base: gm_mg3_base {
        magazineWell[] += {"CBA_762x51_LINKS"};
    };
    // PK
    class gm_pk_base: gm_machineGun_base {
        magazineWell[] += {"CBA_762x54R_LINKS"};
    };
    class gm_pk_veh_base: gm_pk_base
    {
        magazineWell[] += {"CBA_762x54R_LINKS"};
    };
    // PKT
    class gm_pkt_base: gm_pk_base
    {
        magazineWell[] += {"CBA_762x54R_LINKS"};
    };
    class gm_pkt_veh_base: gm_pkt_base
    {
        magazineWell[] += {"CBA_762x54R_LINKS"};
    };
    // MP2A1 (Uzi)
    class gm_mp2_base: gm_rifle_base
    {
        magazineWell[] += {"CBA_9x19_UZI"};
    };
    // AK-47
    class gm_ak47_base: gm_rifle_base
    {
        magazineWell[] += {"CBA_762x39_AK", "CBA_762x39_RPK"};
    };
    // AK-74
    class gm_ak74_base: gm_rifle_base
    {
        magazineWell[] += {"CBA_545x39_AK", "CBA_556x45_RPK"};
    };
    // G3
    class gm_g3_base: gm_rifle_base
    {
        magazineWell[] += {"CBA_762x51_G3", "CBA_762x51_G3_L", "CBA_762x51_G3_XL"};
    };
    // RPG-7
    class gm_rpg7_base: gm_launcher_base
    {
        magazineWell[] += {"CBA_RPG7"};
    };
};
